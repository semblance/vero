require 'spec_helper'

describe Vero::Config do
  before :each do
    @config = Vero::Config.new
  end

  it "should be async by default" do
    @config.async.should be_true
  end

  describe "#auth_token" do
    it "should return nil if either api_key or secret are not set" do
      @config.api_key = nil
      @config.secret = "abcd"
      @config.auth_token.should be_nil

      @config.api_key = "abcd"
      @config.secret = nil
      @config.auth_token.should be_nil

      @config.api_key = "abcd"
      @config.secret = "abcd"
      @config.auth_token.should_not be_nil
    end

    it "should return an expected auth_token" do
      @config.api_key = "abcd1234"
      @config.secret = "efgh5678"
      @config.auth_token.should == "YWJjZDEyMzQ6ZWZnaDU2Nzg="
    end
  end

  describe "#request_params" do
    it "should return a hash of auth_token and development_mode if they are set" do
      @config.api_key = nil
      @config.secret = nil
      @config.development_mode = nil
      @config.request_params.should == {}

      @config.api_key = "abcd1234"
      @config.secret = "abcd1234"
      @config.request_params.should == {auth_token: "YWJjZDEyMzQ6YWJjZDEyMzQ="}

      @config.development_mode = true
      @config.request_params.should == {auth_token: "YWJjZDEyMzQ6YWJjZDEyMzQ=", development_mode: true}
    end
  end

  describe "#domain" do
    it "should return www.getvero.com when not set" do
      @config.domain.should == 'www.getvero.com'
      @config.domain = 'blah.com'
      @config.domain.should_not == 'www.getvero.com'
    end

    it "should return the domain value" do
      @config.domain = 'test.unbelieveable.com.au'
      @config.domain.should == 'test.unbelieveable.com.au'
    end
  end

  describe "#development?" do
    it "should return the value set in development_mode" do
      @config.development_mode = true
      @config.development?.should be_true
    end

    it "should return true when development_mode is false but Rails.env is either development or test" do
      @config.development_mode = false

      stub_env('development') {
        @config.development?.should be_true
      }

      stub_env('test') {
        @config.development?.should be_true
      }

      stub_env('production') {
        @config.development?.should be_false
      }
    end
  end
end