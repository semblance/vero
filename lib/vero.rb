require 'rails'

module Vero 
  autoload :Config,     'vero/config'
  autoload :App,        'vero/app'
  autoload :Trackable,  'vero/trackable'
end